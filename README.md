# Monorepo CI and Secure example

Monorepo with Flask and Typescript.

- [Flask](https://gitlab.com/gitlab-org/security-products/demos/flask)
- [TypeScript](https://gitlab.com/gitlab-org/security-products/tests/typescript-yarn)

- [Flask Vulnerabilty Report](https://gitlab.com/gitlab-org/security-products/demos/flask/-/security/vulnerability_report)
- [TypeScript Vulnerability Report](https://gitlab.com/gitlab-org/security-products/tests/typescript-yarn/-/security/vulnerability_report)
